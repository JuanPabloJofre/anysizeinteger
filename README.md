# README #

AnySizeInteger is a proof of concept library
for doing integer math with no restriction
on the size of the integer.

It's written in C#, since is the language I'm
most confortable with.
Also, since this is a proof of concept of
the algorithms, C# seems to be well suited
for prototyping. 

## Status

[![Build status](https://ci.appveyor.com/api/projects/status/f4fvwf4o8dahfcp0?svg=true)](https://ci.appveyor.com/project/JuanPabloJofre/anysizeinteger)

## What is this repository for? ###

### Quick summary

AnySizeInteger is defined as a class to do 
arithmetic on integers; 
with no restriction on the size of the integer. 

### Version

Current version is 0.0.1.

## How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

## Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

## Who do I talk to? ###

* Repo owner or admin
* Other community or team contact